# Python Simulation of Piwars Items

Setup a python3 virtual env, and install the requirements. 

Run with:

    pgzrun barrel_world.py
        or  simple_demo.py
        or world.by

* A bit disorganised at the moment, but there are 4 categories of python modules here:
    * Worlds:
      -  Entry points, actual things to run
            * barrel_world.py
            * world.py
            * simple_demo.py
      - Todo - these could do with refactoring, in a way that fits with the pgz environment.
    * Simulation Components - in lib
      - Robot, definitions of stuff.
            * categories.py
            * robot.py    -> Definition of robot with body modelling, sensor
    * Utilities - in behaviours folder.
        * pid_controller.py (adapted a little from code in my Learn Robotics Programming Book)
    * Robot Behaviours - in the behaviours folder.
        - Based on the concepts established in Learn Robotics Programming, these are adapted to the simulations.
        - Things for the robot to do:
            * drive_forward_behaviour.py
            * stop_at_wall_behaviour.py
            * wall_hugging_behaviour.py
            * avoid_single_sensor.py

Random Notes/Design and roadmap
---
2 rectangles with colour.
n barrels with colour - mixed up
barrels need to be pushable

start with vector addition. use pygame vector2.

Aim is to run this:

world.py <behaviour_script>
    it prepares the world.
    imports the behaviour and gives it the right robot class
    runs the behaviour in the world.
    drawing things while it does it.
    Behaviour gets:

        behaviour = Behaviour(Robot)
        running = behaviour.run()
        behaviour.Update(timestamp)

Will need adapting from  pgz to pygame perhaps for that?
    

storys:
* get a robot that moves in simulation - set motors. drive forward behaviour - running this imports world, robot and then plays robot behaviour. - done
* Robot avoids walls - done.
* add the colour rectangles - detectable, but robot doesn't collide with them. - done
* robot drive to "safe" - sim1, robot drive to "unsafe" - sim 2.
* Add a barrel. Test - when a vector is pushing it - make it move.
* Robot to find barrel and push it.
* robot to push barrel to a coloured rectangle.
* 2 barrels. Different colour
* close barrels - accidental push simulated
* more barrels - get all into a rect. give up once each is passed rect line.
* Avoid accidental barrel pushing.

---
Image:
Tank image credit must go to Kenny.nl.

---
Links:
* https://pygame-zero.readthedocs.io/en/stable/builtins.html#actors
* http://www.pymunk.org/en/latest/pymunk.html#pymunk.Space
* https://github.com/viblo/pymunk/blob/master/examples/basic_test.py

* https://github.com/slembcke/Chipmunk2D/blob/master/src/cpSpace.c
* https://chipmunk-physics.net
* https://pygame-zero.readthedocs.io/en/stable/builtins.html#rect
* http://www.pymunk.org/en/latest/pymunk.pygame_util.html
* http://www.pymunk.org/en/latest/pymunk.html#pymunk.Shape

---


PID info tuning:
* https://controlstation.com/derivative-affect-pid-controller-performance/
* https://robotics.stackexchange.com/questions/167/what-are-good-strategies-for-tuning-pid-loops

For small, low torque motors with little or no gearing, one procedure you can use to get a good baseline tune is to probe it's response to a disturbance.

To tune a PID use the following steps:

* Set all gains to zero.
* Increase the P gain until the response to a disturbance is steady oscillation.
* Increase the D gain until the the oscillations go away (i.e. it's critically damped).
* Repeat steps 2 and 3 until increasing the D gain does not stop the oscillations.
* Set P and D to the last stable values.
* Increase the I gain until it brings you to the setpoint with the number of oscillations desired (normally zero but a quicker 
* https://github.com/br3ttb/Arduino-PID-Library/blob/master/PID_v1.cpp
* http://support.motioneng.com/downloads-notes/tuning/pid_overshoot.htm