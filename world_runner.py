from importlib import import_module
import sys

import pymunk, pygame
import pymunk.pygame_util

from lib.robot import Robot


class WorldRunner:
    def __init__(self):
        self.space = pymunk.Space()
        self.screen = None
        self.draw_options = None
        self.running_behaviour = None
        self.robot = None
        self.world = None

    def update(self):
        try:
            next(self.running_behaviour)
        except StopIteration:
            pass
        self.robot.update(self.space)
        self.space.step(0.4)

    def draw(self):
        self.screen.fill((1.0, 0, 0))
        self.space.debug_draw(self.draw_options)
        self.robot.draw(self.screen)
        pygame.display.flip()

    def main(self):
        print(sys.argv)
        pygame.init()
        pymunk.pygame_util.positive_y_is_up = False

        behaviour_module = import_module("behaviours." + sys.argv[1])
        world_module = import_module("worlds." + sys.argv[2])

        self.screen = pygame.display.set_mode((world_module.WIDTH, world_module.HEIGHT))
        self.draw_options = pymunk.pygame_util.DrawOptions(self.screen)

        self.robot = Robot()

        self.space.damping = 0.2
        self.space.add(self.robot.body, self.robot.shape)

        self.running_behaviour = behaviour_module.Behaviour(self.robot.controls).run()
        self.world = world_module.World(self.space, self.robot)

        running = True
        while running:
            self.update()
            self.draw()
            self.world.update()
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False


if __name__ == '__main__':
    runner = WorldRunner()
    runner.main()
