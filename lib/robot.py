from pygame import Vector2, draw
import pymunk
import math

from lib.distance_sensor import DistanceSensor
from . import categories


class Controls:
    def __init__(self, robot, dist_mid):
        self.left_speed = 0
        self.right_speed = 0
        self.robot = robot
        self.dist_mid = dist_mid
    
    def set_left(self, speed):
        self.left_speed = speed

    def set_right(self, speed):
        self.right_speed = speed

    def stop(self):
        self.left_speed = 0
        self.right_speed = 0
    

class Robot:
    width = 42
    height = 46


    def __init__(self):
        self.velocity = Vector2(0, 0)
        self.body = pymunk.Body()
        self.shape = pymunk.Poly.create_box(self.body, size=(self.width, self.height))
        self.filter = categories.robot_filter
        self.shape.filter = self.filter
        self.shape.density = 0.1
        # print(f"robot shape is {self.shape}")
        self.dist_mid = DistanceSensor(self, pymunk.Vec2d(0, self.height / 2), 0)
        self.controls = Controls(self, self.dist_mid)

    def draw(self, surface):
        self.dist_mid.draw(surface)

    def update(self, space):
        # print(f"left: {self.controls.left_speed},{self.controls.right_speed}" )
        self.body.apply_force_at_local_point(
            (0, (20 * self.controls.left_speed )),
            (10, 0)
        )
        self.body.apply_force_at_local_point(
            (0, (20 * self.controls.right_speed )),
            (-10, 0)
        )
        self.dist_mid.update(space)
