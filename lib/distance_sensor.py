import math

import pymunk
from pygame import draw

from lib import categories


class DistanceSensor(pymunk.Poly):
    """Object sensor - perform a shape query.
    Since our sensors are poll based - and not callback based - we need to
    perhaps query the space with the shape every time.
    We also need to transform this based on the robot position every time.
    """
    def __init__(self, robot, position, angle):
        """Position is relative to the robot body centre, as is angle"""
        self.depth = 120
        self.depth_sq = self.depth ** 2
        self.field_of_view = 50 // 2
        vertices = [
            pymunk.Vec2d(0, 0),
            pymunk.Vec2d(0, self.depth).rotated_degrees(-self.field_of_view),
            pymunk.Vec2d(0, self.depth).rotated_degrees(self.field_of_view)
        ]
        transformed = [v.rotated(angle) + position for v in vertices]
        self.sensor_origin = pymunk.Vec2d(0, 0).rotated(angle) + position
        super().__init__(robot.body, transformed)
        self.sensor = True
        self.nearest = self.depth
        self.robot_body = robot.body
        self.shape_filter = robot.filter

    def transformed_vertice(self, rel_vertice):
        return rel_vertice.rotated(self.body.angle) + self.body.position

    def draw(self, surface):
        transformed_coordinates = [ self.transformed_vertice(v) for v in self.get_vertices() ]
        draw.polygon(surface,
            (255, 0, 0, 128),
            [v.int_tuple for v in transformed_coordinates]
        )
        tr_sensor_origin = self.transformed_vertice(self.sensor_origin)
        draw.circle(surface, (255, 255, 0, 255), tr_sensor_origin.int_tuple, 2)

    def update(self, space):
        self.nearest = self.depth
        dist_sq_nearest = self.depth_sq
        tr_sensor_origin = self.transformed_vertice(self.sensor_origin)
        # print(f"Sensor origin is {tr_sensor_origin}")
        # Sweep across field of view
        for n in range(-self.field_of_view, self.field_of_view):
            # Rotate by the body angle. Convert to radians
            query_angle = self.body.angle + math.radians(n)
            # Create a vector down with the depth, rotated by the angle
            query_line = pymunk.Vec2d(0, self.depth).rotated(query_angle)
            # Make a segment query, from the sensor origin, to sensor origin + segment
            query_result = self.robot_body.space.segment_query_first(
                tr_sensor_origin,
                tr_sensor_origin + query_line,
                1,
                categories.sensor_filter)
            if query_result:
                # print(f"Got result. Point is {query_result.point}")
                # Get sq distance from other point (to keep it fast, keep sq)
                dist_sq = tr_sensor_origin.get_dist_sqrd(query_result.point)
                # But only store if it's less than nearest.
                if dist_sq < dist_sq_nearest:
                    dist_sq_nearest = dist_sq
        # If we have a nearest, get the sqrt, and store it.
        if dist_sq_nearest < self.depth_sq:
            self.nearest = math.sqrt(dist_sq_nearest)
            # print(f"nearest is {self.nearest}")

    def get_distance(self):
        return self.nearest