from pymunk import ShapeFilter

ROBOT_CATEGORY = 0b1
MARKER_CATEGORY = 0b01
SENSOR_CATEGORY = 0b001

marker_filter = ShapeFilter(
    categories=MARKER_CATEGORY,
    mask=ShapeFilter.ALL_MASKS ^ (ROBOT_CATEGORY | SENSOR_CATEGORY))

robot_filter = ShapeFilter(categories=ROBOT_CATEGORY,
    mask=ShapeFilter.ALL_MASKS ^ (SENSOR_CATEGORY | MARKER_CATEGORY))

sensor_filter = ShapeFilter(categories=SENSOR_CATEGORY,
    mask=ShapeFilter.ALL_MASKS ^ (ROBOT_CATEGORY | MARKER_CATEGORY))
